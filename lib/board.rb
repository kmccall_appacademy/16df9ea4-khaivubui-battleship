require 'byebug'
class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play # return a pos array
    puts 'please input coords for where you would like to attack'
    pos = gets.chomp.delete('[] ').split(',').map(&:to_i)
  end
end

class Board
  attr_reader :grid

  def initialize(grid = nil) # initialize with a new grid
    @grid = grid if grid
    @grid = Board.default_grid unless grid
  end

  def self.default_grid # return 10x10 array
    @grid = Array.new(10) { Array.new(10) }
  end

  def count # number of ships on the board
    count = 0
    grid.each do |row|
      count += row.count(:s)
    end
    count
  end

  def empty?(pos = nil) # check if a pos is empty or entire board is empty
    return grid[pos.first][pos.last].nil? if pos
    return true if grid.all? { |row| row.all?(&:nil?) } && !pos
    false
  end

  def full? # return true if NO box is nil
    return true if grid.all? { |row| row.none?(&:nil?) }
    false
  end

  def place_random_ship # replace a nil with an :s (randomly)
    raise 'ERROR: board is full' if full?
    old_ship_count = count
    while count == old_ship_count
      rand_row = rand(0..grid.length - 1)
      rand_col = rand(0..grid.first.length - 1)
      grid[rand_row][rand_col] = :s if empty?([rand_row, rand_col])
    end
    nil
  end

  def won? # true if :s doesn't exist on the board
    grid.all? { |row| row.none? { |box| box == :s } }
  end

  def mark(pos) # mark a position with :x
    grid[pos.first][pos.last] = :x
  end

  def [](pos) # take an array of 2, return the value at that pos
    grid[pos.first][pos.last]
  end

end

class BattleshipGame
  attr_reader :player, :board

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(pos) # mark a pos with :x
    board.mark(pos)
  end

  def count # return number of ships on board
    board.count
  end

  def game_over? # true if :s doesn't exist on the board (same as board.won)
    board.won?
  end

  def play_turn # ask for a pos then attack that pos
    attack(player.get_play)
  end

end
